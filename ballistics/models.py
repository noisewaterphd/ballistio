from django.db import models
from accounts.models import UserProfile

from model_utils.models import TimeStampedModel

from settings import RATINGS_CHOICES


class Calibers(TimeStampedModel):
    class Meta:
        db_table = 'calibers'

    added_by = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=255)


class Powders(TimeStampedModel):
    class Meta:
        db_table = 'powders'

    added_by = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=255)


class Primers(TimeStampedModel):
    class Meta:
        db_table = 'primers'

    maker = models.ForeignKey(ComponentMakers, unique=True)
    added_by = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=255)


class Cases(TimeStampedModel):
    class Meta:
        db_table = 'cases'

    maker = models.ForeignKey(ComponentMakers, unique=True)
    added_by = models.ForeignKey(UserProfile)
    headstamp = models.CharField(max_length=255)


class Bullets(TimeStampedModel):
    class Meta:
        db_table = 'bullets'

    maker = models.ForeignKey(ComponentMakers, unique=True)
    caliber = models.ForeignKey(Calibers)
    added_by = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=255)
    grains = models.IntegerField(blank=False, null=False, default=230)
    bullet_type = models.ForeignKey(BulletTypes)
    rating = models.IntegerField(choices=RATINGS_CHOICES, default=5)
    comments = models.TextField(default='None')


class BulletTypes(TimeStampedModel):
    class Meta:
        db_table = 'bullet_types'

    STYLE_CHOICES = ('SWC', 'RN', 'FP', 'HP', 'XTP', 'HBRN', 'RS')
    TYPE_CHOICES = ('Jacketed', 'Plated', 'Lead Cast', 'Lead Swaged')

    added_by = models.ForeignKey(UserProfile, unique=True)
    name = models.CharField(max_length=255)
    style = models.CharField(choices=STYLE_CHOICES)
    type = models.CharField(choices=TYPE_CHOICES)


class ComponentMakers(TimeStampedModel):
    class Meta:
        db_table = 'component_makers'

    added_by = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=255)
    rating = models.IntegerField(choices=RATINGS_CHOICES, default=5)
    comments = models.TextField(default='None')

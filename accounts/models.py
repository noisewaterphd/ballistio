__author__ = 'camronlevanger'
import datetime

from django.db import models
from django.contrib.auth.models import User
from django.contrib.localflavor.us.us_states import STATE_CHOICES

from model_utils.models import TimeStampedModel


class UserProfiles(TimeStampedModel):
    user = models.ForeignKey(User, unique=True)
    address = models.ForeignKey(Addresses)
    phone = models.CharField(max_length=255)
    gender = models.CharField(max_length=140)
    birth_date = models.DateField(null=False, blank=False)
    bio = models.CharField(max_length=256)
    accepted_terms = models.BooleanField(default=False)
    accepted_date = models.DateField(null=False, blank=False)
    enabled = models.BooleanField(default=True)
    last_login = models.DateTimeField(default=datetime.datetime.now())
    profile_picture = models.ImageField(upload_to='thumbpath', blank=True)

class Addresses(TimeStampedModel):
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    state = models.CharField(choices=STATE_CHOICES)
    postalcode = models.CharField(max_length=255)
    county = models.CharField(max_length=255)
    country = models.CharField(max_length=255)

__author__ = 'camronlevanger'
import datetime

from django.db import models
from model_utils.models import TimeStampedModel

from accounts.models import UserProfile
from settings import RATINGS_CHOICES


class Firearms(TimeStampedModel):
    class Meta:
        db_table = 'firearms'

    profile = models.ForeignKey(UserProfile, unique = True)
    type = models.ForeignKey(FirearmTypes, null=False)
    model = models.ForeignKey(FirearmModels, null=False)
    name = models.CharField(max_length=255, default=model.model)
    serial_number = models.CharField(max_length=255, default='N/A')
    date_purchased = models.DateField(default=datetime.datetime.now())
    price_paid = models.DecimalField(default=0.00)
    rating = models.IntegerField(choices=RATINGS_CHOICES, default=5)
    comments = models.TextField(default='None')



class FirearmTypes(TimeStampedModel):
    class Meta:
        db_table = 'firearm_types'

    added_by = models.ForeignKey(UserProfile)
    type = models.CharField(null=False, blank=False, unique=True,
                            default='%s Owned Gun' % added_by.user.first_name)


class FirearmMakes(TimeStampedModel):
    class Meta:
        db_table = 'firearm_makes'

    type = models.CharField(null=False, blank=False, default='Gun')


class FirearmModels(TimeStampedModel):
    class Meta:
        db_table = 'firearm_models'

    make = models.ForeignKey(FirearmMakes, null=False)
    model = models.CharField(null=False, blank=False, default='Gun')
